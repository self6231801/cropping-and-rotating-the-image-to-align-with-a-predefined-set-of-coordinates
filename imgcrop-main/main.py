import cv2
import numpy as np
from fastapi import FastAPI
from pydantic import BaseModel
import base64

app = FastAPI()

class ImageRequest(BaseModel):
    baseImg: str

def mapp(h):
    h = h.reshape((4,2))
    hnew = np.zeros((4,2), dtype=np.float32)

    add = h.sum(1)
    hnew[0] = h[np.argmin(add)]
    hnew[2] = h[np.argmax(add)]

    diff = np.diff(h, axis=1)
    hnew[1] = h[np.argmin(diff)]
    hnew[3] = h[np.argmax(diff)]

    return hnew

@app.get("/test1")
async def root():
    return {"message": "Hello World"}

@app.post("/cropwithrotation")
async def crop_with_rotation(request_data: ImageRequest):
    base_img = request_data.baseImg

    try:
        with open('image1.png', 'wb') as img_file:
            img_data = base64.b64decode(base_img)
            img_file.write(img_data)

        image = cv2.imread("image1.png")
        image = cv2.resize(image, (1300, 800))
        orig = image.copy()

        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        blurred = cv2.GaussianBlur(gray, (5, 5), 0)
        edged = cv2.Canny(blurred, 30, 50)

        contours, hierarchy = cv2.findContours(edged, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
        contours = sorted(contours, key=cv2.contourArea, reverse=True)

        for c in contours:
            p = cv2.arcLength(c, True)
            approx = cv2.approxPolyDP(c, 0.02 * p, True)

            if len(approx) == 4:
                target = approx
                break
        approx = mapp(target)

        pts = np.float32([[0, 0], [595, 0], [595, 842], [0, 842]])
        op = cv2.getPerspectiveTransform(approx, pts)
        dst = cv2.warpPerspective(orig, op, (595, 842))

        filename = "final.png"
        cv2.imwrite(filename, dst)

        with open(filename, "rb") as f:
            im_b64 = base64.b64encode(f.read())
            output = str(im_b64, 'UTF-8')
            data = {
                "basedata": {
                    "message": output
                }
            }

        return data

    except Exception as e:
        return {"error": str(e)}

if __name__ == '__main__':
    import uvicorn
    uvicorn.run("main:app", port=8000, reload=False)
